#!/bin/sh
#base apps
sudo apt install git mc vim neofetch telegram-desktop vlc-bin lm-sensors jq synaptic cifs-utils timeshift -y

#gnome 
sudo apt install gnome-software gnome-shell-extensions gnome-shell-extension-manager gnome-tweaks remmina -y
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
gsettings set org.gnome.mutter center-new-windows true

#gnome-extensions
#manual install extensions: 
# https://extensions.gnome.org/extension/3906/remove-app-menu/
# https://extensions.gnome.org/extension/517/caffeine/
# https://extensions.gnome.org/extension/355/status-area-horizontal-spacing/
# https://extensions.gnome.org/extension/2182/noannoyance/
# https://extensions.gnome.org/extension/1460/vitals/

#remove snap
sudo systemctl stop snapd.service
sudo systemctl stop snapd.socket
sudo systemctl stop snapd.service
sudo systemctl stop snapd.apparmor.service
sudo systemctl stop snapd.seeded.service
sudo systemctl disable snapd.service
sudo systemctl disable snapd.socket
sudo systemctl disable snapd.service
sudo systemctl disable snapd.apparmor.service
sudo systemctl disable snapd.seeded.service
sudo apt autoremove --purge snapd -y
sudo rm -rf /var/cache/snapd/
sudo rm -rf $HOME/snap

#chrome
sudo wget -O- https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | sudo tee /usr/share/keyrings/google-chrome.gpg
echo deb [arch=amd64 signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt update -y
sudo apt install google-chrome-stable -y

#dotnet
sudo apt install dotnet6 -y

#pass
sudo apt install pass gnome-pass-search-provider kleopatra wl-clipboard qtpass -y

#virtualization virtualbox + qemu-kvm + virt-manager
sudo apt install virtualbox -y
sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virt-manager -y
sudo adduser $USER libvirt
sudo systemctl enable --now libvirtd
sudo upgrade -y

#logitech mouse
#sudo apt install solaar -y

#docker docker-compose
sudo apt install docker docker-compose -y
sudo usermod -aG docker $USER

#flatpack
sudo apt install flatpak gnome-software-plugin-flatpak gnome-software -y
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#flatpack apps
sudo flatpak install smartgit rider app/com.microsoft.Teams/x86_64/stable app/com.visualstudio.code/x86_64/stable com.skype.Client us.zoom.Zoom io.dbeaver.DBeaverCommunity -y

sudo reboot
